package com.huatec.hiot_cloud.test.mvptest.dagger2test;

import com.huatec.hiot_cloud.test.mvptest.TestMVPActivity;

import dagger.Component;

@Component(modules = TestModule.class)
public interface PresenterComponent {
    void inject(TestMVPActivity testMVPActivity);
}
